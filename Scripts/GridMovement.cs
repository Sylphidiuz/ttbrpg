﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GridMovement:MonoBehaviour
{
    public EnvironementManager ev;
    public Fighter fighter;
    public GameObject wayPoint;
    public bool find;
    public GameObject currentPath; // GameObject contenant les autres GameObjects affichant le chemin pris.
    public float speed = 5f;
    protected Rigidbody2D rb;
    public bool buzzy = false;
    public virtual void TakePath(Queue<Vector2Int> path)
    {
        buzzy = true;
        fighter.Buzzy = true;
        StartCoroutine(FollowPath(path));
    }
    protected virtual void DrawPath(Queue<Vector2Int> path)
    {
        if (path != null)
            DeletePath();
        currentPath = new GameObject("path");
        while(path.Count > 0)
        {

        }
    }
    protected virtual void DeletePath()
    {
        Destroy(currentPath);
    }

    public bool Buzzy()
    {
        return buzzy;
    }

    protected IEnumerator FollowPath(Queue<Vector2Int> path)
    {
        Vector2Int precedent = path.Peek();
        Queue<Vector2Int> road = new Queue<Vector2Int>();
        Vector2Int curent = Vector2Int.zero;
        while(path.Count > 0)
        {
            Vector2Int head = path.Dequeue();
            if (precedent != head)
            {
                road.Enqueue(curent);
                curent = Vector2Int.zero;
                precedent = head;
            }
            curent += head;
        }
        road.Enqueue(curent);
        while (road.Count > 0)
        {
            Vector2Int offset = road.Dequeue();
            Vector3 destination = transform.position + new Vector3(offset.x, offset.y);
            Vector3 sens = new Vector3(offset.x, offset.y).normalized;
            while(Vector3.Magnitude(destination - transform.position) >= 0.1f)
            {
                rb.MovePosition(transform.position + sens * Time.deltaTime * speed);
                yield return null;
                fighter.Position = new Vector2Int(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
            }
            transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        }
        buzzy = false;
        fighter.Buzzy = false;
    }
}
