﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum EType
{
    ally,
    neutral,
    opponent,
    summonA,
    summonO,
    empty,

}

//Toute entité en jeu avec des pv pouvant prendre des actions
public abstract class Fighter:ITarget,IActor
{
    protected Vector2Int position;
    protected EType type;
    protected bool haveTurn;
    protected int hp;
    protected int atk;
    protected int def;
    protected int speed;
    protected int movPoint;
    protected int hpMax;
    protected int atkMod;
    protected int defMod;
    protected int speedMod;
    protected int movPointMod;
    protected List<Action> actions;
    protected bool canMove = true;
    protected bool buzzy = false;
    public Fighter(int hpV= 1 ,int atkV =1, int defV=1, int speedV=1,
        int movPointV=1, int hpMaxV=1, int atkModV=1, int defModV=1, int speedModV=1, int movPointModV=0,  EType typeV = EType.neutral)
    {
        actions = new List<Action>();
        actions.Add(new Attack());
        hp = hpV;
        atk = atkV;
        def = defV;
        speed = speedV;
        movPoint = movPointV;
        hpMax = hpMaxV;
        atkMod = atkModV;
        defMod = defModV;
        speedMod = speedModV;
        movPointMod = movPointModV;
        type = typeV;
    }
    public void AddActions(List<Action> list)
    {
        actions.AddRange(list);
    }
    public void AddActions(Action list)
    {
        actions.Add(list);
    }
    public EType Type { get => type; set => type = value; }
    public bool HaveTurn { get => haveTurn; set => haveTurn = value; }
    public int Hp { get => hp; set => hp = value; }
    public int Atk { get => atk; set => atk = value; }
    public int Def { get => def; set => def = value; }
    public int Speed { get => speed; set => speed = value; }
    public int MovPoint { get => movPoint; set => movPoint = value; }
    public int HpMax { get => hpMax; set => hpMax = value; }
    public int AtkMod { get => atkMod; set => atkMod = value; }
    public int SpeedMod { get => speedMod; set => speedMod = value; }
    public int MovPointMod { get => movPointMod; set => movPointMod = value; }
    public int DefMod { get => defMod; set => defMod = value; }
    public Vector2Int Position { get => position; set => position = value; }
    public List<Action> Actions { get => actions; }
    public bool CanMove { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public bool Buzzy { get => buzzy; set => buzzy=value; }
}

//Toute entité en jeu liée à une autre entité
public class Invocation: Fighter
{
    private Character summoner;

    public Invocation(Character summoner, int hpV = 1, int atkV = 1, int defV = 1, int speedV = 1,
        int movPointV = 1, int hpMaxV = 1, int atkModV = 1, int defModV = 1, int speedModV = 1, int movPointModV = 0, EType typeV = EType.neutral): base(hpV,atkV,defV,speedV,movPointV,hpMaxV,atkModV,defModV,speedModV,movPointModV,typeV)
    {
        this.summoner = summoner;
    }
}

//Toute entité en jeu prenant part au combat
public class Character : Fighter
{
    public Character( int hpV = 1, int atkV = 1, int defV = 1, int speedV = 1,
        int movPointV = 1, int hpMaxV = 1, int atkModV = 1, int defModV = 1, int speedModV = 1, int movPointModV = 0, EType typeV = EType.neutral) : base(hpV, atkV, defV, speedV, movPointV, hpMaxV, atkModV, defModV, speedModV, movPointModV, typeV)
    {
    }
}

//Interface abstraite regroupant les pts communs entre ITarget et IActor
public interface IAct
{
     EType Type { get; set; }
     bool HaveTurn { get; set; }
     int Hp { get; set; }
     int Atk { get; set; }
     int Def { get; set; }
     int Speed { get; set; }
     int MovPoint { get; set; }
     int HpMax { get; set; }
     int AtkMod { get; set; }
     int DefMod { get; set; }
     int SpeedMod { get; set; }
     int MovPointMod { get; set; }
     Vector2Int Position { get; set; }
    bool CanMove { get; set; }
    bool Buzzy { get; set; }
}

//Interface pour récuperer les valeurs d'une cible d'action
public interface ITarget:IAct
{
}

//Interface pour récuperer les valeurs d'une entité perpetrant une action
public interface IActor : IAct
{
    List<Action> Actions { get ; }
}

public abstract class Action
{
    protected string name;
    protected string description;
    protected int range;
    protected int minRange;
    protected int nTarget;
    public Action() { }
    public abstract void Execution(IActor acteur, List<ITarget> cibles);
    public abstract bool VerityTarget(ITarget cible, int numeroCible);
    public string Name{ get => name;}
    public string Description { get => name; }
    public int Range { get => range; }
    public int MinRange { get => minRange; }
    public int NTarget { get => range; }
}

// Une ITarget Empty pour quand on cible une case vide. Cette target n'a pas de stats,il ne faut donc pas appeler autre chpse que sa position ou son type
public class TargetDummy : ITarget
{
    protected Vector2Int pos;
    public EType Type { get => EType.empty; set => throw new System.NotImplementedException(); }
    public bool HaveTurn { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int Hp { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int Atk { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int Def { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int Speed { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int MovPoint { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int HpMax { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int AtkMod { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int DefMod { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int SpeedMod { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public int MovPointMod { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public Vector2Int Position { get => pos; set => pos = value; }
    public bool CanMove { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public bool Buzzy { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    public TargetDummy(Vector2Int pos)
    {
        Position = pos;
    }
}

public class Attack : Action
{
    public Attack():base() {
        name = "Attaque";
        description = "Une simple attaque";
        nTarget = 1;
        range = 1;
        minRange = 1;
    }
    public override void Execution(IActor acteur, List<ITarget> cibles)
    {
        if (cibles.Count == 1)
        {
            Debug.Log("Hp avant coup: " + cibles[0].Hp);
            cibles[0].Hp -= acteur.Atk * acteur.AtkMod - cibles[0].Def * cibles[0].DefMod;
            Debug.Log("des hp ont été perdus : " + cibles[0].Hp);
        }
        else Debug.Log("il manque une cible");
    }

    public override bool VerityTarget(ITarget cible, int numeroCible)
    {
        if (numeroCible == 0)
        {
            return cible.Type != EType.empty;
        }
        else return false;
    }
}
