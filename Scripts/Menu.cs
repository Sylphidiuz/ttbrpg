﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Menu : MonoBehaviour
{
    public bool basemenuvisible = true;
    public bool actMenuVisible = false;
    public GameObject basemenu;
    public GameObject actionMenu;
    public GameObject button;
    public EnvironementManager ev;
    public List<GameObject> actions = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
       basemenu.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate 
       {
           ev.SetMove();
           basemenu.SetActive(false);
           basemenuvisible = false;
       });
        basemenu.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(delegate
        {
            ActionMenu();
            basemenu.SetActive(false);
            basemenuvisible = false;
        });
        basemenu.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(delegate
        {
            ev.TurnEnd();
        });

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (actMenuVisible)
            {
                QuitActionMenu();
                ev.QuitAction();
            } else 
            if (!basemenuvisible)
            {
                basemenu.SetActive(true);
                basemenuvisible = true;
                ev.QuitMove();
            }
            
        }
        
    }
    public void ReturnMenu()
    {
        basemenu.SetActive(true);
        basemenuvisible = true;
    }
    public void ActionMenu()
    {
        actMenuVisible = true;
        actionMenu.SetActive(true);
        IActor chara = ev.GetFighter();
        for (int i = 0; i < chara.Actions.Count; i++)
        {
            Action act = chara.Actions[i];
            GameObject action = Instantiate(button,actionMenu.transform.position + new Vector3(0,  - i*50  - 50),Quaternion.identity,actionMenu.transform);
            action.GetComponentInChildren<Text>().text = chara.Actions[i].Name;
            action.GetComponent<Button>().onClick.AddListener(delegate {
                ev.SetAction(act);
            });
            actions.Add(action);
        }
    }
    public void QuitActionMenu()
    {
        actMenuVisible = false;
        actions.ForEach(x => Destroy(x));
        actions.Clear();
        ReturnMenu();
        actionMenu.SetActive(false);
    }
}
