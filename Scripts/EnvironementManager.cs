﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironementManager : MonoBehaviour
{
    public LayerMask pathLayer;
    public LayerMask targetLayer;
    public GameObject targetTile;
    public MapManager map;
    public List<GameObject> tiles = new List<GameObject>();
    public List<GridMovement> characters = new List<GridMovement>();
    public Dictionary<GameObject, KeyValuePair<IActor, ITarget>> chara = new Dictionary<GameObject, KeyValuePair<IActor, ITarget>>();
    public List<GameObject> timeline = new List<GameObject>();
    public int characterTurn;
    public bool start = true;
    public bool move = false;
    public bool wait = false;
    public bool act = false;
    public bool display = false;
    public Menu menu;
    public Action action;
    public List<ITarget> targets = new List<ITarget>();
    // Start is called before the first frame update
    void Start()
    {
        characterTurn = 0;
    }
    public Fighter GetFighter() { return characters[characterTurn].fighter; }
    public IActor GetActor() { return chara[timeline[characterTurn]].Key; }
    public ITarget GetTarget() { return chara[timeline[characterTurn]].Value;}
    public IActor GetActor(int pos) { return chara[timeline[pos]].Key; }
    public ITarget GetTarget(int pos) { return chara[timeline[pos]].Value; }
    public GameObject GetObject() { return timeline[characterTurn]; }
    //Turn basé sur characters et ses fighters
    public void BaseTurn()
    {
        if (wait)
        {
            if (!characters[characterTurn].Buzzy())
            {
                wait = false;
                menu.ReturnMenu();
            }
        }
        else
        if (move)
        {
            Move();
        }
        else
        if (act)
        {
            Act();
        }
    }
    public void TestTurn()
    {
        if (wait)
        {
            if (!GetActor().Buzzy)
            {
                wait = false;
                menu.ReturnMenu();
            }
        }
        else
        if (move)
        {
            Move();
        }
        else
        if (act)
        {
            Act();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        TestTurn();        
    }


    public List<Vector2Int> GetBlockingCharacters(Fighter chara)
    {
        List<Vector2Int> result = new List<Vector2Int>();
        for(int i = 0; i < characters.Count; i++)
        {
            if (!AreAllies(characters[i].fighter.Type,chara.Type))
            {
                result.Add(characters[i].fighter.Position);
            }
        }
        return result;
    }

    public List<Vector2Int> GetBlockingCharacters(IActor charac)
    {
        List<Vector2Int> result = new List<Vector2Int>();
        List<KeyValuePair<IActor, ITarget>> gens = new List<KeyValuePair<IActor, ITarget>>(chara.Values);
        for (int i = 0; i < gens.Count; i++)
        {
            if(gens[i].Key != null)
            {
                if (!AreAllies(gens[i].Key.Type, charac.Type))
                {
                    result.Add(gens[i].Key.Position);
                }
            } else
            {
                if (!AreAllies(gens[i].Value.Type, charac.Type))
                {
                    result.Add(gens[i].Value.Position);
                }
            }
            
        }
        return result;
    }

    private bool AreAllies(EType type, EType type2)
    {
        if(type == EType.ally || type == EType.summonA)
        {
            if (type2 == EType.ally || type2 == EType.summonA)
                return true;
            return false;
        }
        if (type == EType.opponent || type == EType.summonO)
        {
            if (type2 == EType.opponent || type2 == EType.summonO)
                return true;
            return false;
        }
        if (type == EType.neutral)
        {
            if (type2 == EType.neutral)
                return true;
            return false;
        }
        return false;
    }

    void DisplayPathRange(int range) {
        for (int i = - range; i <= range; i++)
        {
            for (int j = - range; j <= range; j++)
            {
                if(Mathf.Abs(i) + Mathf.Abs(j) <= range && Mathf.Abs(i) + Mathf.Abs(j) != 0 && map.LimitedAstar(range,GetObject().GetComponent<GridMovement>(), GetObject().transform.position + new Vector3(i, j)))
                {
                    tiles.Add(Instantiate(targetTile, GetObject().transform.position + new Vector3(i,j),Quaternion.identity));
                }
            }
        }
        display = true;
    }

    void DisplayActionRange()
    {
        for (int i = -action.Range; i <= action.Range; i++)
        {
            for (int j = -action.Range; j <= action.Range; j++)
            {
                if (Mathf.Abs(i) + Mathf.Abs(j) <= action.Range && Mathf.Abs(i) + Mathf.Abs(j) >= action.Range)
                {
                    tiles.Add(Instantiate(targetTile,GetObject().transform.position + new Vector3(i, j), Quaternion.identity));
                }
            }
        }
        display = true;
    }

    void DeleteDisplayRange()
    {
        display = false;
        tiles.ForEach(x =>
            Destroy(x)
            );
        tiles.Clear();
    }

    bool MoveVerityTarget()
    {
        
        Vector3Int ok = map.map.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)+ new Vector3(0.5f,0.5f));
        Vector3 cellPos = map.map.CellToWorld(ok);
        //Debug.Log(cellPos);
        if (Physics2D.OverlapCircle(new Vector2(cellPos.x, cellPos.y), 0.5f, pathLayer))
        {
            Debug.Log(true);
            return true;
        }
        Debug.Log(false);
        return false;
    }
    void Move()
    {
        if (!display)
        {
            DisplayPathRange(GetActor().MovPoint + GetActor().MovPointMod);
        }
        if (Input.GetMouseButtonDown(0) && MoveVerityTarget())
        {
            GetObject().GetComponent<GridMovement>().TakePath(map.AstarQueu(GetObject().GetComponent<GridMovement>(), Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0.5f, 0.5f)));
            wait = true;
            QuitMove();
        }
    }
    public void SetMove()
    {
        move = true;
    }
    public void QuitMove()
    {
        move = false;
        DeleteDisplayRange();
    }
    public void SetAction(Action actio)
    {
        act = true;
        action = actio;
        targets.Clear();
        DisplayActionRange();
    }
    public void QuitAction()
    {
        act = false;
        
        DeleteDisplayRange();
    }
    public void Act()
    {
        if (targets.Count != action.NTarget)
        {
            if (Input.GetMouseButtonDown(0) && MoveVerityTarget())
            {
                ActionVerifyTarget();
            }

        }
        else
        {
            action.Execution(GetActor(), targets);
            timeline.Sort(Initiative);
            QuitAction();
        }
    }

    private int Initiative(GameObject x, GameObject y)
    {
        return chara[x].Key.Speed * chara[x].Key.SpeedMod > chara[y].Key.Speed * chara[y].Key.SpeedMod ? 1 : -1;
    }

    // Ca marche pas !
    public void ActionVerifyTarget() {
        Vector3Int ok = map.map.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0.5f, 0.5f));
        Vector3 cellPos = map.map.CellToWorld(ok);
        Collider2D[] salut = new Collider2D[1];
        if (Physics2D.OverlapCircle(new Vector2(cellPos.x, cellPos.y), 0.5f, targetLayer))
        {
            ContactFilter2D cf = new ContactFilter2D();
            cf.SetLayerMask(targetLayer);
            cf.useLayerMask = true;
            Physics2D.OverlapCircle(new Vector2(cellPos.x, cellPos.y), 0.5f, cf, salut);
            //salut est vide
            ITarget ciblu = chara[salut[0].gameObject].Value;
            if (ciblu != null && action.VerityTarget(ciblu, targets.Count))
            {
                targets.Add(ciblu);
            }
        }
        else if (action.VerityTarget(new TargetDummy(new Vector2Int(Mathf.RoundToInt(cellPos.x), Mathf.RoundToInt(cellPos.y))), targets.Count)) targets.Add(new TargetDummy(new Vector2Int(Mathf.RoundToInt(cellPos.x), Mathf.RoundToInt(cellPos.y))));
    }
    public void TurnEnd()
    {
        timeline.Sort(Initiative);
        if (characterTurn < timeline.Count - 1)
            characterTurn++;
        else
            characterTurn = 0;
    }
    public int Initiative(GridMovement a, GridMovement b)
    {
        return a.fighter.Speed * a.fighter.SpeedMod > b.fighter.Speed * b.fighter.SpeedMod ? 1 : -1;
    }
}
