﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamManager : MonoBehaviour
{
    public EnvironementManager target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(target.characters[target.characterTurn].transform.position.x, target.characters[target.characterTurn].transform.position.y, transform.position.z);
    }
}
