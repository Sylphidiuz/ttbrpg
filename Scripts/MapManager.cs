﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapManager : MonoBehaviour
{
    public Tilemap map;
    public GridMovement hox;
    List<List<bool>> Grille;
    List<List<bool>> VraiGrille;
    // Start is called before the first frame update
    void Start()
    {
        
        Grille = new List<List<bool>>();
        for (int i = 0; i < map.size.x; i++)
        {
            Grille.Add(new List<bool>());
            for (int j = 0; j < map.size.y; j++)
            {
                Grille[i].Add(map.GetTile(new Vector3Int(i, j, 0)));
            }
        }
        
        Astar(hox, hox.transform.position + new Vector3(0, 3, 0));

    }

    // Update is called once per frame
    void Update()
    {

    }
    public class Noeud
    {
        public int x;
        public int y;
        public int cout;
        public float heuristique;
        public Noeud nxt;
        public Noeud(int ex, int ey, int ecout = 0, float heur = 0)
        {
            x = ex;
            y = ey;
            cout = ecout;
            heuristique = heur;
        }

    }
    public bool LimitedAstar(int limit, GridMovement req, Vector3 destination)
    {
        req.find = false;
        Vector3Int dest = map.WorldToCell(destination);
        List<Vector2Int> res = new List<Vector2Int>();
        List<Noeud> closedList = new List<Noeud>();
        List<Noeud> openList = new List<Noeud>();
        Vector3Int cell = map.WorldToCell(req.transform.position);
       
        openList.Add(new Noeud(cell.x, cell.y));
        openList[0].heuristique = Vector2Int.Distance(new Vector2Int(cell.x, cell.y), new Vector2Int(dest.x, dest.y));
        while (openList.Count != 0)
        {
            Noeud u = openList[0];
            openList.RemoveAt(0);
            if (dest.x == u.x && dest.y == u.y)
            {
                req.find = true;
                return true;
            }
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if ((j != 0 || i != 0) && (i * j == 0) && u.x + i >= 0 && u.y + j >= 0 && u.x + i < map.size.x && u.y + j < map.size.y && Grille[u.x + i][u.y + j])
                    {
                        Noeud v = new Noeud(u.x + i, u.y + j);
                        v.nxt = u;
                        v.cout = u.cout + 1;
                        v.heuristique = v.cout + Vector2Int.Distance(new Vector2Int(v.x, v.y), new Vector2Int(dest.x, dest.y));
                        if (closedList.Find(x => x.x == v.x && x.y == v.y) == null && (openList.Find(x => x.x == v.x && x.y == v.y) == null || openList.Find(x => x.x == v.x && x.y == v.y).cout + openList.Find(x => x.x == v.x && x.y == v.y).heuristique > v.cout + v.heuristique) && v.cout <= limit)
                        {
                            Noeud item = openList.Find(x => x.x == v.x && x.y == v.y);
                            if (item != null)
                            {
                                openList.Remove(item);
                            }
                            openList.Add(v);
                        }
                        else
                        {

                        }
                    }
                }

            }
            closedList.Add(u);
        }
        return false;
    }
    List<Vector2Int> Astar(GridMovement req, Vector3 destination) {
        req.find = false;
        Vector3Int dest = map.WorldToCell(destination);
        List<Vector2Int> res = new List<Vector2Int>();
        List<Noeud> closedList = new List<Noeud>();
        List<Noeud> openList = new List<Noeud>();
        Vector3Int cell = map.WorldToCell(req.transform.position);
       
        openList.Add(new Noeud(cell.x, cell.y));
        openList[0].heuristique= Vector2Int.Distance(new Vector2Int(cell.x, cell.y), new Vector2Int(dest.x, dest.y));
        while (openList.Count != 0)
        {
            Noeud u = openList[0];
            openList.RemoveAt(0);
            if (dest.x == u.x && dest.y == u.y)
            {
                req.find = true;
                return DepilerChemin(u, cell, req, closedList);
            }
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if ( (j != 0 || i != 0)&&( i*j == 0) && u.x + i >= 0 && u.y + j >= 0 && u.x + i < map.size.x && u.y + j < map.size.y && Grille[u.x + i][u.y + j])
                    {
                        Noeud v = new Noeud(u.x + i, u.y + j);
                        v.nxt = u;
                        v.cout = u.cout + 1;
                        v.heuristique = v.cout + Vector2Int.Distance(new Vector2Int(v.x, v.y), new Vector2Int(dest.x, dest.y));
                        if (closedList.Find(x => x.x == v.x && x.y == v.y) == null && (openList.Find(x => x.x == v.x && x.y == v.y) == null || openList.Find(x => x.x == v.x && x.y == v.y).cout + openList.Find(x => x.x == v.x && x.y == v.y).heuristique > v.cout + v.heuristique)) {
                            Noeud item = openList.Find(x => x.x == v.x && x.y == v.y);
                            if(item != null)
                            {
                                openList.Remove(item);
                            }
                            openList.Add(v);
                        }
                        else
                        {
                            
                        }
                    }
                }
                
            }
            closedList.Add(u);
        }
        return readClosedList(closedList);
    }
    public Queue<Vector2Int> AstarQueu(GridMovement req, Vector3 destination)
    {
        List<Vector2Int> res = Astar(req, destination);
        Queue<Vector2Int> result  = new Queue<Vector2Int>(res);
        return result;
    }
    private List<Vector2Int> readClosedList(List<Noeud> list)
    {
        List<Vector2Int> res = new List<Vector2Int>();
        for (int i = 0; i < list.Count; i++)
        {
            res.Add(new Vector2Int(list[i].x, list[i].y));
            
        }
        return res;
    }

    private List<Vector2Int> DepilerChemin(Noeud u,Vector3Int start,GridMovement req , List<Noeud> list)
    {
        
        List<Vector2Int> res = new List<Vector2Int>();
        while(u.x != start.x || u.y != start.y)
        {
            Vector3 worldPos = map.CellToWorld(new Vector3Int(u.x, u.y, 0));
            res.Add(new Vector2Int(u.x - u.nxt.x,u.y - u.nxt.y));
            
            u = u.nxt;
        }
        res.Reverse();
        return res;
    }
    public List<List<bool>> GetGrid() { return Grille; }
}
    
                    
        
        

