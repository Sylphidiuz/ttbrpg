﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : GridMovement
{
    public Queue<Vector2Int> path = new Queue<Vector2Int>();
    
    // Start is called before the first frame update
    void Start()
    {
        ev.characters.Add(this);
        rb = GetComponent<Rigidbody2D>();
        fighter = new Character();
        fighter.MovPoint = 5;
        ev.timeline.Add(gameObject);
        ev.chara.Add(gameObject, new KeyValuePair<IActor, ITarget>(fighter, fighter));
    }

    // Update is called once per frame
    void Update()
    {
        if (find && path.Count!= 0)
        {
            find = false;
            TakePath(path);
        }
    }
}
